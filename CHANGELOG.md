<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Changelog

All notable changes will be documented in this file.

## [Unreleased]

## [1.0.0] - 2020-04-21

Initial release of the workshop material.

[unreleased]: https://gitlab.com/hifis/hifis-workshops/introduction-to-git-and-gitlab/workshop-material/compare/1.0.0...master
[1.0.0]: https://gitlab.com/hifis/hifis-workshops/introduction-to-git-and-gitlab/workshop-material/-/tags/1.0.0
