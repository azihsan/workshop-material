<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

SPDX-License-Identifier: CC-BY-4.0
-->

# Using the Shell

In this episode, we prepare our environment to get started with the Git commandline client.
In addition, we introduce you to the most important commands.

## Start Git Bash

### Windows 10

- Open [Explorer]
  - Show and explain directory tree
  - Select Desktop -> [right click] -> "Git Bash here"
- Explain where you can find the options (menu `Properties`, font etc.)
- Explain how to copy and paste (quick edit mode)

### UNIX-like Operating System

- Open a shell of your choice

## Overview about basic Shell Commands

- Explain the basic concept "No news are good news."
  => commands only report information when something went wrong
- Introduce basic commands: cd, ls, cat, less, rm, pipe
- For more details, please see the [detailed version of the shell introduction](https://swcarpentry.github.io/shell-novice/) and see the [shell cheat sheet](extras/shell-cheat-sheet.docx). 

## Key Points

- The shell (CLI) is a program to run other programs.
- "No news are good news."
- (Most) CLI programs can `--help` you.
